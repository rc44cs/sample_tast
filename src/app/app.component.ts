import { Component } from '@angular/core';
import { EmployeeService } from './shared/service/employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bootstrap-design';
  constructor(
    private employeeService: EmployeeService
  ){
      this.employeeService.getEmployeeDetails().subscribe(res => {
        localStorage.setItem('list',JSON.stringify(res.employees) );
        // this.EmpolyeeList = res.employees;
      })
  }
}
