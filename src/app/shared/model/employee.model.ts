export class EmployeeModel {
    firstName: string = '';
    id: number = 0;
    lastName: string = '';
    position: string = '';
    salary: number = 0;
    officeAddress: OfficeAddressModel = new OfficeAddressModel();
    mainAddress: MainAddressModel = new MainAddressModel();
}

export class OfficeAddressModel {
    officeAddress1: string = '';
    officeAddress2: string = '';
    officeCity: string = '';
    officeState: string = '';
    officeCountry: string = '';
}



export class MainAddressModel {
    mainAddress1: string = '';
    mainAddress2: string = '';
    mainCity: string = '';
    mainState: string = '';
    mainCountry: string = '';
}