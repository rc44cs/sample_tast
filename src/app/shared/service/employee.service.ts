import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OnInit, Component, Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable({
    providedIn: 'root'
})

export class EmployeeService {
    employeeDetails: any;
    constructor(private httpClient: HttpClient) {
        this.getEmployeeDetails();
    }



    getEmployeeDetails() {

        const url = '../../../assets/json/employee-data.json';
        return this.httpClient.get(url).map((response: any) => {
            return response;
        });
    }

}