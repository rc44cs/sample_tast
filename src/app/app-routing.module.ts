import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeDetailsListComponent } from './employee-details/employee-details-list/employee-details-list.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';


const routes: Routes = [
  { path: '', redirectTo: 'employee-details-list', pathMatch: 'full' },
  { path: 'employee-details-list', component: EmployeeDetailsListComponent },
  { path: 'employee-details', component: EmployeeDetailsComponent },
  {
    path: 'employee-details/:id', component: EmployeeDetailsComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
