import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/shared/service/employee.service';
import { EmployeeModel } from 'src/app/shared/model/employee.model';


@Component({
  selector: 'app-employee-details-list',
  templateUrl: './employee-details-list.component.html',
  styleUrls: ['./employee-details-list.component.css']
})
export class EmployeeDetailsListComponent implements OnInit {
  EmpolyeeList: EmployeeModel[];
  term:string;
  constructor(private router: Router,
    private employeeService: EmployeeService
  ) { }

  ngOnInit() {
    this.getEmployeeDetailsList();
  }
  addNewEmployee() {
    this.router.navigate(['employee-details']);
  }

  getEmployeeDetailsList() {
    // this.employeeService.getEmployeeDetails().subscribe(res => {
      
      this.EmpolyeeList = JSON.parse(localStorage.getItem('list'));
      // this.EmpolyeeList = res.employees;
    // })
  }

  onEmployeeDeatilsEdit(empolyee){
    this.router.navigate(['employee-details', empolyee.id]);
  }



}
