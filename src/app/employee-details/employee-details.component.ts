import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared/service/employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeModel } from '../shared/model/employee.model';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css'],
})
export class EmployeeDetailsComponent implements OnInit {
  selectedEmployeeId;
  isFilledAllRequiredField: boolean;
  employeeModel: EmployeeModel = new EmployeeModel();
  constructor(private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router, 
    
    ) { }

  ngOnInit() {
    this.isFilledAllRequiredField = true;
    this.route.params.subscribe(result => {
      if (Object.keys(result).length !== 0) {
        if (result.id) {
          const employeeList = JSON.parse(localStorage.getItem('list'))
          employeeList.filter(emp => {
            if (emp.id === +result.id) {
              this.employeeModel = emp;
            }
          })

        }
      }
    });
  }

  ngOnChanges() {
    this.isFilledAllRequiredField = true;
  }

  validate(formStatus){
    if (!this.employeeModel.firstName || !this.employeeModel.lastName || !this.employeeModel.position ||
       !this.employeeModel.salary ) {
      this.isFilledAllRequiredField = false;
      return;
    }

    this.onSaveEmployee();
  }

 
 

  onSaveEmployee() {
    if(this.employeeModel.id) {
      const employeeList = JSON.parse(localStorage.getItem('list'));
      employeeList.filter(emp => {
        if (emp.id === this.employeeModel.id) {
          Object.assign(emp, this.employeeModel)
        }
      });
      this.storeToLocalStorage(employeeList)
    } else {
      const employeeList = JSON.parse(localStorage.getItem('list'));
      this.employeeModel.id = employeeList.length +1
      employeeList.push(this.employeeModel);
     this.storeToLocalStorage(employeeList)
    }
  
  }

  storeToLocalStorage(employeeList?){
    localStorage.removeItem('list')
    localStorage.setItem('list', JSON.stringify(employeeList));
    this.back();
  }

  delete(employeeModel){
    const employeeList = JSON.parse(localStorage.getItem('list'))
    const index = employeeList.findIndex(p => +p.id === +employeeModel.id);
    employeeList.splice(index,1);
    this.storeToLocalStorage(employeeList)
  }

  back(){
    this.router.navigate(['employee-details-list']);
  }



}
